MODULES USED:
- Nodemon

START APP:
- $npm run build
- $npm run dev

MYSQL DATA:
need to have installed mysql with a connection established

CODE ORGANIZATION:
server/build -> Translate of typeScript code to javaScript
server/src/controllers -> controllers of the backend
server/src/routes -> routes of the controllers (POST, GET, etc...)
server/src/keys.ts -> information of the DB connection
server/src/database.ts -> where we create connection to the DB


VIDEO EXAMPLE: 
https://www.youtube.com/watch?v=lxYB79ANJM8