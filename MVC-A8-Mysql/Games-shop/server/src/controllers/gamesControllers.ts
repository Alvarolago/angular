import {Request, Response } from 'express';

import connection from '../database';

class OddsController {
    public async list (req: Request, res: Response){
        const houses = connection.query('SELECT * FROM GAMES');
        console.log(houses);
    }

    public async create (req: Request, res: Response): Promise<void> {
        await connection.query("INSERT INTO GAMES set ?",[req.body]);
        //console.log(req.body);
        res.json({message: 'Casa añadida'});
    }

    public delete (req: Request, res: Response){
        res.json({text: 'deleting a game' + req.params.id});
    }

    public async getOne(req: Request, res: Response): Promise<void> {
        const {id} = req.params;
        const house = await connection.query('SELECT * FROM GAMES WHERE id = ?', [id]);
        console.log(house);
    }


}

const oddsController = new OddsController();
export default oddsController;