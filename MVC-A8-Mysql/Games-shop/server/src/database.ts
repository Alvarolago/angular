import keys from './keys';

var mysql = require('mysql');
let connection = mysql.createConnection(keys.database);

export default connection;