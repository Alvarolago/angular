import express, {Application} from 'express';
import indexRoutes from './routes/indexRoutes';
import morgan from 'morgan';
import cors from 'cors';
import oddsRoute from './routes/gamesRoute';

class Server {

    public app: Application;

    constructor(){
        this.app = express();
        this.config();
        this.routes();
    }

    config(): void {
        this.app.set('port', 3500);     //Indicamos si existe un purto en caso contrario ejecuta el 3000
        this.app.use(morgan("dev"));    //Con morgan nos muestra las peticiones en el commandshell
        this.app.use(cors());
        this.app.use(express.json());   //Para poder aceptar formatos json
        this.app.use(express.urlencoded({extended:false}))
    }

    routes(): void {
        this.app.use(indexRoutes);
        this.app.use('/api/odds', oddsRoute);
    }

    start(): void {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port: ', this.app.get('port'));
        });
    }

}

const server = new Server();
server.start();