import { Router } from 'express';

import oddsController from '../controllers/gamesControllers';

class IndexRoutes {
    public router: Router = Router();

    constructor(){
        this.config();
    }

    config(): void {
        this.router.get('/', oddsController.list);
        this.router.post('/', oddsController.create);
        this.router.delete('/:id', oddsController.delete);
    }
}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router;