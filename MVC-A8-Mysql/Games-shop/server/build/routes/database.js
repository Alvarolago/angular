"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const promise_mysql_1 = __importDefault(require("promise-mysql"));
const keys_1 = __importDefault(require("./keys"));
var pool;
promise_mysql_1.default.createConnection(keys_1.default.database).then(connection => {
    pool = connection;
    console.log('BD IS CONNECTED');
});
exports.default = pool;
