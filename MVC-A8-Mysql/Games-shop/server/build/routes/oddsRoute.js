"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const oddsControllers_1 = __importDefault(require("../controllers/oddsControllers"));
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', oddsControllers_1.default.list);
        this.router.post('/', oddsControllers_1.default.create);
        this.router.delete('/:id', oddsControllers_1.default.delete);
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
